deployed at http://146.185.144.46:8080/secureREST

try free resource at http://146.185.144.46:8080/secureREST/resteasy/publichello/{name}

then try secured resources at http://146.185.144.46:8080/secureREST/resteasy/hello/{name}
and http://146.185.144.46:8080/secureREST/resteasy/hello/authenticated/user

you will be asked for login/password (I have 2 defined adm:aaa and fed:fff) as far
as in spring-security next <intercept-url>s defined:

    <http auto-config="true" use-expressions="true">
        <intercept-url pattern="/resteasy/hello/**" access="isAuthenticated()" />
        <intercept-url pattern="/admin/**" access="hasRole('ROLE_ADM')" />
    </http>

At the same time SpringMVC app here mapped on /

http://146.185.144.46:8080/secureREST  returns welcome page (free to access)
http://146.185.144.46:8080/secureREST/admin requires to be in admin role

There is a form on admin page. Method of controller mapped on form submit (POST). This method calls REST at
http://146.185.144.46:8080/secureREST/resteasy/hello/authenticated/user and returns REST response in the admin page