/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fed.info.springapp.resteasy;

import java.security.Principal;
import java.util.Collection;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author fed
 */
@Path("hello")
public class HelloResource {
    
    @GET
    @Path("{name}")
    @Produces(MediaType.TEXT_HTML)
    public String getHello(@PathParam("name") String name, @Context SecurityContext sc) {
        StringBuilder sb = new StringBuilder();
        sb.append("<h3> HELLO ").append(name).append("<h3>");
        
        sb.append("<h4>sc = @Context SecurityContext = ").append(sc).append("</h4>");
        Principal p = sc != null ? sc.getUserPrincipal() : null;
        String pName = p!= null ? p.getName() : "null";
        sb.append("<h4>sc.getUserPrincipal().getname() : ").append(pName).append("</h4>");
        boolean  isInRole_ROLE_USER = sc != null ? sc.isUserInRole("ROLE_USER") : false;
        sb.append("<h4>sc.isUserInRole(\"ROLE_USER\") : ").append(isInRole_ROLE_USER).append("</h4><br>");

        org.springframework.security.core.context.SecurityContext springSc = SecurityContextHolder.getContext();
        sb.append("<h4>springSc = SecurityContextHolder.getContext() = ").append(springSc).append("</h4>");
        
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>)springSc.getAuthentication().getAuthorities();
        sb.append("<h4>springSc.getAuthentication().getAuthorities() : ").append(authorities).append("</h4>");
        String username = springSc.getAuthentication().getName();
        sb.append("<h4>springSc.getAuthentication().getName() : ").append(username).append("</h4>");
        
        return sb.toString();
    }
    
    @GET
    @Path("/authenticated/user")
    @Produces(MediaType.TEXT_HTML)
    public String getHelloForUser(@Context SecurityContext sc) {
        String name = sc.getUserPrincipal().getName();
        return "HELLO "+ name;
    }
    
    
}
