<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
    <head>
        <script type="text/javascript">
            function createRequest() {
                var result = null;
                if (window.XMLHttpRequest) {
                    // FireFox, Safari, etc.
                    result = new XMLHttpRequest();
                } else if (window.ActiveXObject) {
                    // MSIE
                    result = new ActiveXObject("Microsoft.XMLHTTP");
                } else {
                    // No known mechanism -- consider aborting the application
                }
                return result;
            }

            function send_get_request() {
                var div = document.getElementById('responsediv');
                div.innerHTML = div.innerHTML + "... ";
                var req = createRequest();
                req.onreadystatechange = function () {
                    if (req.readyState != 4)
                        return; // Not there yet
                    if (req.status != 200) {
                        // Handle request failure here...
                        div.innerHTML = div.innerHTML + "error " + req.status;
                        return;
                    }
                    // Request successful, read the response
                    var resp = req.responseText;
                    // ... and use it as needed by your app.
                    div.innerHTML = div.innerHTML + resp;
                };
                req.open("GET", "<c:url value="resteasy/hello/authenticated/user"/>", true);
                
                req.send();                
                
            }
        </script>        
        
    </head>
    <body>
        
        <h1>Title : ${title}</h1>
        <h1>Message : ${message}</h1>
        <c:if test="${what != null}">
            <h4>What requested : ${what}</h4>
        </c:if>
        <c:if test="${restresponse != null}">
            <h4>REST response : ${restresponse}</h4>
        </c:if>

        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <h2>Welcome : ${pageContext.request.userPrincipal.name} 
                | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a></h2>  
            </c:if>

        <form id="requestform" action="admin" method="post">
            <input id="whatfield" name="what" value="">
            <button>Send request</button>            
        </form>

            <input id="restreqbtn" type="button" onclick="send_get_request()" value="Send GET request with JS!"/>        
        <div id="responsediv">response here : </div>
    </body>
</html>