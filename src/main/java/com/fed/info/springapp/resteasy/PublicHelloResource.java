/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fed.info.springapp.resteasy;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author fed
 */
@Path("publichello")
public class PublicHelloResource {
    
@GET
    @Path("{name}")
    @Produces(MediaType.TEXT_HTML)
    public String getPublicHello(@PathParam("name") String name) {
        return "<h3> PUBLIC HELLO " + name + "<h3>";
    }
}
