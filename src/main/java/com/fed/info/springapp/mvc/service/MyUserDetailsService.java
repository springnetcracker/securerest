
package com.fed.info.springapp.mvc.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 *
 * @author fed
 */
public class MyUserDetailsService implements UserDetailsService {

    private final Map<String, UserInfo> users = new HashMap<>();

    {
        users.put("fed", new UserInfo("fed", "fff", "ROLE_USER"));
        users.put("adm", new UserInfo("adm", "aaa", "ROLE_ADM"));
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        UserInfo u = users.get(userName);
        if (u == null) {
            throw new UsernameNotFoundException("Incorrect Credentials");
        }
        UserDetails ud = new User(u.getLogin(), u.getPasswd(), true, true, true, true,
                Arrays.asList(new GrantedAuthority[]{
            new GrantedAuthorityImpl(u.getRole())
        }));
        return ud;
    }

    private static class UserInfo {

        private final String login;
        private final String passwd;
        private final String role;

        UserInfo(String login, String passwd, String role) {
            this.login = login;
            this.passwd = passwd;
            this.role = role;
        }

        /**
         * @return the login
         */
        public String getLogin() {
            return login;
        }

        /**
         * @return the passwd
         */
        public String getPasswd() {
            return passwd;
        }

        /**
         * @return the roles
         */
        public String getRole() {
            return role;
        }

    }

}
